using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using RazorPoc.Data;
using RazorPoc.Model;

namespace RazorPoc.Pages.Products;

public class CreateModel : PageModel
{
    private readonly ApplicationDbContext _dbContext;

    [BindProperty]
    public Product Product { get; set; }
    
    public CreateModel(ApplicationDbContext dbContext)
    {
        _dbContext = dbContext;
    }
    
    public IActionResult OnGet()
    {
        // Tutaj trzeba by było ładowac rzeczy np. do selectow etc.
        return Page();
    }

    public async Task<IActionResult> OnPostAsync()
    {
        var emptyProduct = new Product();
        emptyProduct.Id = Guid.NewGuid();

        await TryUpdateModelAsync(emptyProduct, "product", p => p.Name, p => p.Quantity);
        
        _dbContext.Products.Add(emptyProduct);
        await _dbContext.SaveChangesAsync();

        return RedirectToPage("Index");
    }
}