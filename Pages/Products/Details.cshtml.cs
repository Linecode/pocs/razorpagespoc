using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using RazorPoc.Data;
using RazorPoc.Model;

namespace RazorPoc.Pages.Products;

public class Details : PageModel
{
    private readonly ApplicationDbContext _dbContext;

    public Product Product { get; set; }

    public Details(ApplicationDbContext dbContext)
    {
        _dbContext = dbContext;
    }
    
    public async Task<IActionResult> OnGetAsync(Guid id)
    {
        Product = await _dbContext.Products.FirstOrDefaultAsync(p => p.Id.Equals(id));

        return Page();
    }
}