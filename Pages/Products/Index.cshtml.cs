using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using RazorPoc.Data;
using RazorPoc.Model;

namespace RazorPoc.Pages.Products;

public class Index : PageModel
{
    private readonly ApplicationDbContext _dbContext;

    public IList<Product> Products;

    public Index(ApplicationDbContext dbContext)
    {
        _dbContext = dbContext;
    }
    
    public async Task OnGetAsync()
    {
        Products = await _dbContext.Products.AsNoTracking().ToListAsync();
    }
}